#!/bin/sh
cd B3SScanSDK.framework;
read -p "请输入SDK版本串(x.y.z.w):" VERSION
while [  -z $VERSION ]
do
    read -p "请输入SDK版本串:" VERSION
done

appledoc --no-create-docset --company-id "com.body3dscale" -p "B3SScanSDK" -v "$VERSION" -c "body3dscale.com" -o ../doc  Headers
