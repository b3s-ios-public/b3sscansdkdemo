#import <UIKit/UIKit.h>

@interface UIView (B3S)

- (UIImage *)snapshotImageWithScale:(CGFloat)scale;

@end
