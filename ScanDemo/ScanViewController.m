//
//  ScanViewController.m
//

#import "ScanViewController.h"
#import "B3SLocalizedString.h"
#import "MeshViewController.h"
#import <B3SScanSDK/B3SScanSDK.h>
#import <ReactiveCocoa.h>
#import "B3SSpeechService.h"
#import <Toast/Toast.h>

#define ScanInterval 3    // one-way time of turning head
#define ScanUpInterval 2  // one-way time of raising up head 

@interface ScanViewController ()

@property (nonatomic, strong) B3SFaceReport *faceReportCopy;
@property (nonatomic, strong) B3SScanner *scanner;
@property (weak, nonatomic) IBOutlet UIView *renderView;
@property (nonatomic, assign) BOOL isVideoMirrored;

// timer for playing guidance instructions to the user.
@property (nonatomic, strong) dispatch_source_t scanTimer;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UIButton *plasticButton;

@property (nonatomic, strong) NSString *modelPath;
@end

@implementation ScanViewController

+ (instancetype)new
{
  NSBundle *bundle = [NSBundle bundleForClass:self.class];
  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" 
                                                       bundle:bundle];
   return [storyboard instantiateViewControllerWithIdentifier:@"IDScanVC"];
}

- (void)dealloc
{
  // release scanner
  _scanner=nil;
  _faceReportCopy=nil;
  [self destroyObservers];
  NSLog(@"ScanViewController dealloc.");
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self changeTitleColor:[UIColor blackColor]];
  self.faceReportCopy=[B3SFaceReport new];
  _isVideoMirrored=NO;
  // setup scanner
  [self setupScanManager];
  [self initObservers];
  NSLog(@"ScanViewController viewDidLoad.");
}

- (void)setupScanManager
{
  self.scanner = [B3SScanner new];
  //configure some scan settings here
  //here we set the working distance range to normal reading distance.
  self.scanner.scanSettings.minDistance=0.32f;
  self.scanner.scanSettings.maxDistance=0.50f;
  self.scanner.scanSettings.enable106Ldmk=YES;
  // specify the required callback for scanner
  [self setupScanManagerCallback];
}


- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  // start camera, attach a preview layer to scan manager
  [self openCameraAndStartStream];
  // tell the user to look straight at the camera NOW.
  self.title=B3SLocalizedString(@"speech.lookatcamera",nil);
  [[B3SSpeechService sharedInstance] speak:self.title force:NO];
  NSLog(@"ScanViewController viewDidAppear.");
}

- (void)viewDidDisappear:(BOOL)animated
{
  [super viewDidDisappear:animated];
  
  dispatch_async(dispatch_get_main_queue(), ^{
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
  });
  // close camera
  [self closeCameraAndStopStream];
  // invoke cancel: before the user leaves this view controller.
  [self.scanner cancel:^{
    NSLog(@"scanning cancelled.");
  }];
  NSLog(@"ScanViewController viewDidDisappear.");
}

- (void)setupScanManagerCallback
{
  __weak typeof(self) wself = self;
  
  // implement a callback to get informed when scanner finished all work.
  // Be Careful: this callback will be invoked on Main thread!
  self.scanner.finishHandleWithModelPath = ^(NSString *modelDir, NSError * error)
  {

      if (!error) {
          NSLog(@"the final model files are saved to:%@",modelDir);
          wself.modelPath = modelDir;
          [wself presentMeshViewer:modelDir];
      }

  };
  
  // implement a callback to capture some rare internal error of scanner.
  self.scanner.abortWithError = ^(int code, NSString *desc) 
  {
    __strong typeof(wself) sself=wself;
    NSLog(@"scanner aborted! with code:%d,desc:%@",code,desc);
    
    // guide the user to start over
    NSString * msg=[NSString stringWithFormat:
      @"Sorry, but an error(code:%d) happened, please retry scanning",code];
      
    UIAlertController *alertController = 
      [UIAlertController alertControllerWithTitle:@"Oops!" 
                                         message:msg
                                  preferredStyle:UIAlertControllerStyleAlert];
                                                                      
    UIAlertAction *action = 
        [UIAlertAction actionWithTitle:@"OK" 
                                 style:UIAlertActionStyleDefault 
                               handler:^(UIAlertAction * _Nonnull action) {
                               
      #if 1
      //now you can restart the camera, then the user can click scan button again. 
      [sself.scanner restartCamera];
      //Don't forget to reset the voice instructions or any UI to the initial 
      //state as if the user enters this view controller for the first time.
      if(sself.scanTimer) {dispatch_cancel(sself.scanTimer);sself.scanTimer=nil;}
      sself.title=B3SLocalizedString(@"speech.startoverscan",nil);
      [[B3SSpeechService sharedInstance] speak:sself.title force:YES];
      #else
      // Or if it's too cumbersome to reset the UI to the initial state, 
      // you can simply cancel this session and close this viewcontroller.
      // let the user to enter this viewcontroller again.
      [sself closeCameraAndStopStream];
      [sself.scanner cancel:^{
        [sself.navigationController popViewControllerAnimated:YES];
        NSLog(@"cancel callback done.");
      }];
      #endif
    }];
    
    [alertController addAction:action];
    [sself presentViewController:alertController animated:YES completion:NULL];
  };
}

// MARK: ==open & close camera==

-(void) openCameraAndStartStream
{
  // specify a layer of app's UIView for previewing camera video stream
  [self.scanner setCameraPreviewRootLayer:self.renderView.layer];
  // open camera and prepare resources for scanning.
  // `mirror` argument defaults to NO. you can change to YES if you like.
  [self.scanner startCamera:self.isVideoMirrored withCompletion:^(NSError *error) {
    if(error){
      //couldn't open camera(see the documentation of `startCamera`)
      __weak typeof(self) wself=self;
      dispatch_async(dispatch_get_main_queue(), ^{
        //here please inform your user
        __strong typeof(wself) sself=wself;
        sself.title=error.localizedDescription;
      });
    }
  }];
}

-(void) closeCameraAndStopStream
{
  // close camera, detach the preview layer from app's UIView.
  [self.scanner stopCamera];
}

// MARK: ==start & stop scan==

/**
 * respond to the user clicking 'Start Scan'
 */
- (IBAction)tapStart:(id)sender
{
  // 1. start scanning. refer to the documentation of `startScan` method.
  NSError *error=[self.scanner checkIfCouldStart];
  if (error) {
    [self.view makeToast:error.localizedDescription duration:2.0 position:CSToastPositionCenter];
    return ;
  }
  //play the first voice instruction. This is optional.
  self.title=B3SLocalizedString(@"speech.beginscan",nil);
  [[B3SSpeechService sharedInstance] speak:self.title force:NO];
  //then start scanning
  [self.scanner startScan];
  
  // 2. play instructions to guide the user turning his/her head.
  __block float turnLeft = 9999.0;
  __block float turnRight = 9999.0;
  __block float turnUp = 2; //when to play 'raise up head' voice instruction
  __weak typeof(self) wself = self;
  
  /*
    Below is an example instruction sequence for `Face+Neck Mode`, in which we
    should instruct the user to first raise up head then back to the frontal face.
    If you just want to use the `Face Mode`, please remove the first
    two voice instructions and just begin with the 'turn left' voice instruction.
   */
  
  // remember the timestamp of starting scan.
  NSDate *beginTime = [NSDate date];
  
  //setup a timer to play the subsequential instructions at specific timestamp.
  dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
  dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC, 0 * NSEC_PER_SEC);
  dispatch_source_set_event_handler(timer, ^{
    __strong typeof(wself) sself=wself;
    NSTimeInterval interval =  [[NSDate date] timeIntervalSince1970] - [beginTime timeIntervalSince1970];
    int x=round(interval);
    NSLog(@"x= %@",@(x));
    
    //make sure at least 1-second-stay at frontal face, 
    //then begin turning or raising up head.
    if (x == turnUp) {
      
      // now tell the user to raise up head.
      sself.title=B3SLocalizedString(@"speech.raiseup",nil);
      [[B3SSpeechService sharedInstance] speak:sself.title force:NO];
      NSLog(@"%.2lf:Raise Up",interval);
      
    } else if (x==turnUp+ScanUpInterval) { //now at the uppermost position
      
      // tell the user to stop raising up and turn to middle now.
      sself.title=B3SLocalizedString(@"speech.tomiddle",nil);
      [[B3SSpeechService sharedInstance] speak:sself.title force:NO];
      //turnUp = x;
      NSLog(@"%.2lf:Back to Middle",interval);
      
    } else if (x == turnUp+ScanUpInterval*2) {
      turnLeft = x;
      sself.title =B3SLocalizedString(@"speech.turnleft",nil);
      [[B3SSpeechService sharedInstance] speak:sself.title force:NO];
      NSLog(@"%.2lf:Turn Left", interval);
      
    } else if (x == turnLeft + ScanInterval) {//now at leftmost side
      
      sself.title=B3SLocalizedString(@"speech.tomiddle",nil);
      [[B3SSpeechService sharedInstance] speak:sself.title force:NO];
      NSLog(@"%.2lf:Back to Middle", interval);
      
    } else if (x == turnLeft+ScanInterval*2) {
      
      sself.title = B3SLocalizedString(@"speech.turnright",nil);
      [[B3SSpeechService sharedInstance] speak:sself.title force:NO];
      NSLog(@"%.2lf:Turn right", interval);
      
    } else if (x == turnLeft+ScanInterval*3) { //now at the rightmost side 
      
      turnRight = x;
      sself.title = B3SLocalizedString(@"speech.tomiddle",nil);
      [[B3SSpeechService sharedInstance] speak:sself.title force:NO];
      NSLog(@"%.2lf:Back-to-Middle", interval);
      
    } else if (x >= turnLeft+ScanInterval*4) {// now at the middle
      
      sself.title=B3SLocalizedString(@"speech.capturedone",nil);
      [[B3SSpeechService sharedInstance] speak:sself.title force:NO];
      NSLog(@"%.2lf:Capture Finished", interval);
      [sself stopScanning];
    }
  });
  dispatch_resume(timer);;
  
  self.scanTimer = timer;
}


- (void)stopScanning
{
  // cancel the instruction timer.
  dispatch_cancel(self.scanTimer);
  // stop scanning, start postprocessing.
  [self.scanner stopScan];
  // close camera stream
  [self.scanner stopCamera];
  // watch the progress of postprocessing
  [self watchGenModelProgress];
  
}

// MARK: ==watch postprocessing progress==
- (void)watchGenModelProgress 
{
  __weak typeof(self) wself=self;
  [[RACSignal interval:0.2 onScheduler:[RACScheduler mainThreadScheduler]] subscribeNext:^(id x) {
    __strong typeof(wself) sself=wself;
    float prog=sself.scanner.genModelProgress;
    if (prog < 1) {
      sself.title=[NSString stringWithFormat: B3SLocalizedString(@"scanVC.postprog", nil),prog*100];
    } else {
      sself.title = B3SLocalizedString(@"scanVC.postdone",nil);
    }
  }];
}

// MARK: ==Observers==
-(void) initObservers
{
  //observe faceReport
  [self.scanner addObserver:self 
                      forKeyPath:@"faceInfo" 
                      options:NSKeyValueObservingOptionNew 
                      context:NULL];
}

-(void) destroyObservers
{
  [self.scanner removeObserver:self forKeyPath:@"faceInfo"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context
{
  __weak typeof(self) wself=self;
  if([keyPath isEqualToString:@"faceInfo"] && object == self.scanner){
    B3SFaceReport *faceReport = change[NSKeyValueChangeNewKey];
    if(faceReport && ![faceReport isKindOfClass:[NSNull class]]){
      // copy interested fields in faceReport
      self.faceReportCopy.rect=faceReport.rect;
      self.faceReportCopy.imgWidth=faceReport.imgWidth;
      self.faceReportCopy.imgHeight=faceReport.imgHeight;
      self.faceReportCopy.hairInfo=faceReport.hairInfo;
      self.faceReportCopy.roll=faceReport.roll;
      self.faceReportCopy.yaw=faceReport.yaw;
      self.faceReportCopy.pitch=faceReport.pitch;
      self.faceReportCopy.distanceInfo=faceReport.distanceInfo;
      self.faceReportCopy.eulerAnglesInfo=faceReport.eulerAnglesInfo;
      self.faceReportCopy.hairInfo=faceReport.hairInfo;
    }else{
      //faceReport is nil, reset the copy to zero rect.
      self.faceReportCopy.rect=CGRectZero;
      return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^(){
      __strong typeof(wself) sself=wself;
      [sself updateFaceRectViewOnUIThread];
    });
  }
}

// present tips for the user to adjust head pose before scanning
// and feedback the head turning Yaw angle to the user.
-(void)updateFaceRectViewOnUIThread
{
  // B3SScanner keep detecting human face before scanning,
  // if detection succeed, faceReportCopy.rect is non-zero.
  // But during scanning, it doesn't detect human face anymore.
  if(!CGRectEqualToRect(self.faceReportCopy.rect,CGRectZero))
  {
    CGRect previewFrameRect=[self.scanner getCameraPreviewLayerFrameRect];
    CGSize prevFrameSz=previewFrameRect.size;
    //人脸框展示
    //矩形框等比例缩小, 用于在faceboxLayer展示人脸框, faceboxLayer固定高度为500
    const float scaleY =(float) prevFrameSz.height/self.faceReportCopy.imgHeight;
    const float scaleX =(float) prevFrameSz.width/self.faceReportCopy.imgWidth;
    CGRect rect = CGRectMake(self.faceReportCopy.rect.origin.x * scaleX, //x
                             self.faceReportCopy.rect.origin.y * scaleY, //y
                             self.faceReportCopy.rect.size.width * scaleX, //width
                             self.faceReportCopy.rect.size.height * scaleY); //height
    if(!self.isVideoMirrored){
      // if video is mirrored, adjust the facebox's position accordingly 
      CGFloat pw = previewFrameRect.size.width;
      CGFloat rectOrigX = self.faceReportCopy.rect.origin.x * scaleX;
      CGFloat rectOrigY = self.faceReportCopy.rect.origin.y * scaleY;
      CGFloat rectSizeW = self.faceReportCopy.rect.size.width * scaleX;
      CGFloat rectSizeH = self.faceReportCopy.rect.size.height * scaleY;
      CGFloat scale=1;
      rect = CGRectMake(rectOrigX+(rectSizeW-rectSizeW*scale) / 2,//x
             rectOrigY + (rectSizeH-rectSizeH*scale)/2,//y
             rectSizeW * scale, //width
             rectSizeH * scale); //height
      rect.origin.x = pw - rect.size.width - rect.origin.x;
    }

    [self.scanner setCameraPreviewFaceboxRect:rect];
    [self formatFaceInfoTips];
  }
  
  // during scanning, only euler angles(roll/yaw/pitch) of faceReportCopy are valid.
  // light up the dots in the guide bar to indicate the progress of head turning. 
  [self updateGuideBar:self.faceReportCopy];
}

-(void)formatFaceInfoTips
{
  NSString *distMsg=B3SLocalizedString(@"scanVC.before.tips.dist", nil);
  NSString *orientMsg=B3SLocalizedString(@"scanVC.before.tips.orient",nil);
  if(self.faceReportCopy.distanceInfo==0){distMsg=[distMsg stringByAppendingString:B3SLocalizedString(@"scanVC.before.tips.dist.ok",nil)];}
  else if(self.faceReportCopy.distanceInfo==1){distMsg=[distMsg stringByAppendingString:B3SLocalizedString(@"scanVC.before.tips.dist.closer",nil)];}
  else if(self.faceReportCopy.distanceInfo==2){distMsg=[distMsg stringByAppendingString:B3SLocalizedString(@"scanVC.before.tips.dist.further",nil)];}
  
  if(self.faceReportCopy.eulerAnglesInfo==0){orientMsg=[orientMsg stringByAppendingString:B3SLocalizedString(@"scanVC.before.tips.orient.ok",nil)];}
  else if(self.faceReportCopy.eulerAnglesInfo==1){orientMsg=[orientMsg stringByAppendingString:B3SLocalizedString(@"scanVC.before.tips.orient.low",nil)];}
  else if(self.faceReportCopy.eulerAnglesInfo==2){orientMsg=[orientMsg stringByAppendingString:B3SLocalizedString(@"scanVC.before.tips.orient.up",nil)];}
  else if(self.faceReportCopy.eulerAnglesInfo==3||self.faceReportCopy.eulerAnglesInfo==4){
    orientMsg=[orientMsg stringByAppendingString:B3SLocalizedString(@"scanVC.before.tips.orient.look",nil)];
  }
    
  self.title=[distMsg stringByAppendingString:orientMsg];
  bool allOK=self.faceReportCopy.hairInfo==0 &&
             self.faceReportCopy.distanceInfo==0 &&
             self.faceReportCopy.eulerAnglesInfo==0;
  if(allOK){
    [self changeTitleColor:[UIColor blueColor]];
  }else{
    [self changeTitleColor:[UIColor redColor]];
  }
}

// light up the dots to indicate the progress of user's head turning
- (void)updateGuideBar:(B3SFaceReport *)faceReport
{
//  if(![self.scanner isScanStarted]
//    || isnan(faceReport.yaw)) return;
//  
//  // enable the stopScan button only after head turning is almost done
//  if([self.indexForYawID count] > 0){
//      self.stopButton.hidden = NO;
//  }
//  
//  NSArray *subviews = [self.guideView subviews] ;
//  int yawAngle = (int)faceReport.yaw;
//  
//  int index = 0;
//
//  for (UIButton *button in subviews) {
//      [button setImage:[UIImage imageNamed:@"faceGrey"] forState:0];
//  }
//  
//  for (id key in self.indexForYawID) {
//      NSNumber* indexNumber = [self.indexForYawID objectForKey:key];
//      [subviews[[indexNumber intValue]] setImage:[UIImage imageNamed:@"faceGreen"] forState:0];
//  }
//  
//  if (yawAngle > -25 && yawAngle < 25 ) {
//      //at the middle
//      index = 2;
//  } else if (yawAngle >= 25 && yawAngle < 50) {
//      //on the way to the right side
//      index = 3;
//  } else if (yawAngle >= 50) {
//      //at the rightmost
//      index = 4;
//  } else if (yawAngle > -50 && yawAngle <= -25) {
//      //on the way to the left side
//      index = 1;
//  } else if (yawAngle <= -50) {
//      //at the leftmost
//      index = 0;
//  }
//  
//  NSNumber* currentIndex = [self.indexForYawID objectForKey:[NSNumber numberWithInteger:index]];
//  if(currentIndex == nil){
//      [self.indexForYawID setObject:[NSNumber numberWithInteger:index] forKey:[NSNumber numberWithInteger:index]];
//  }
//
//  UIButton *button = subviews[index];
//  [button setImage:[UIImage imageNamed:@"faceRed"] forState:0];
    
}

-(void) changeTitleColor:(UIColor*) color
{
  NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:
                            color,NSForegroundColorAttributeName,
                            [UIFont systemFontOfSize:12],NSFontAttributeName, nil];
  [self.navigationController.navigationBar setTitleTextAttributes:attributes];
}

// MARK: ===go to plastic operation viewcontroller===

- (void)presentMeshViewer:(NSString *)modelDir
{
  //SCNNode *meshNode=[self createMeshNodeFromMeshInfo:model];
  
  // pop this viewcontroller and go to mesh viewcontroller
  MeshViewController *vc=[MeshViewController new];
  vc.modelDir = modelDir;
  NSMutableArray<UIViewController*> *vcs=
  [self.navigationController.viewControllers mutableCopy];
  [vcs replaceObjectAtIndex:vcs.count-1 withObject:vc];
  [self.navigationController setViewControllers:vcs animated:YES];
}

@end
