//
//  MeshViewController.h
//

#import <UIKit/UIKit.h>
#import <SceneKit/SceneKit.h>
#import <ModelIO/ModelIO.h>
#import <SceneKit/ModelIO.h>

@protocol MeshViewDelegate <NSObject>
- (void)meshViewWillDismiss;
- (void)meshViewDidDismiss;
- (BOOL)meshViewDidRequestColorizing:(NSURL*)mesh
            previewCompletionHandler:(void(^)(void))previewCompletionHandler
           enhancedCompletionHandler:(void(^)(void))enhancedCompletionHandler;
@end

@interface MeshViewController : UIViewController

//@property (nonatomic) NSString * modelPath;
@property (nonatomic, assign) id<MeshViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet SCNView *scnView;
@property (nonatomic, copy) NSString *modelDir;

@end
