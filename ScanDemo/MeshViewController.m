//
//  MeshViewController.mm
//
//
#import <Toast/Toast.h>
#import "MeshViewController.h"
#import <B3SScanSDK/B3SScanSDK.h>

@interface MeshViewController()
@property (nonatomic, strong) SCNNode *meshNode;
@end

// MARK: ==Implementation==
@implementation MeshViewController

+ (instancetype)new
{
  NSBundle *bundle = [NSBundle bundleForClass:self.class];
  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" 
                                                       bundle:bundle];
  MeshViewController *meshViewController = 
  (MeshViewController *)[storyboard instantiateViewControllerWithIdentifier:@"IDMeshVC"];
  
  return meshViewController;
}

-(void) dealloc
{
  _meshNode=nil;
  NSLog(@"MeshViewController dealloc");
}

// MARK: ==VC life cycle==
- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.scnView.showsStatistics=YES;
  self.scnView.autoenablesDefaultLighting=NO;
  self.scnView.allowsCameraControl = YES;
  self.scnView.autoenablesDefaultLighting = NO;
  self.scnView.backgroundColor = [UIColor blackColor];
  self.scnView.defaultCameraController.maximumHorizontalAngle=90;
  self.scnView.defaultCameraController.minimumHorizontalAngle=-90;
  self.title = @"Mesh View";
  self.meshNode = [self setupMeshNode];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  
  SCNScene *scene = [SCNScene scene];
  self.meshNode.position = SCNVector3Make(0, 0, 0);
  // x2.2
  self.meshNode.transform = SCNMatrix4MakeScale(2.2, 2.2, 2.2);
  [scene.rootNode addChildNode:self.meshNode];
  
  /// create a camera node
  SCNNode *cameraNode = [SCNNode node];
  cameraNode.camera = [SCNCamera camera];
  cameraNode.camera.projectionDirection=SCNCameraProjectionDirectionHorizontal;
  cameraNode.camera.fieldOfView=50;
  cameraNode.position = SCNVector3Make(0, 0, 1.2);
  [scene.rootNode addChildNode:cameraNode];
  
  self.scnView.scene = scene;
}

-(SCNNode *)setupMeshNode
{
  B3SFaceModel *finalModel=[[B3SFaceModel alloc] initModelPath:self.modelDir];
  return [self createMeshNodeFromMeshInfo:finalModel];
}

// create a scenekit node from final model.
-(SCNNode*) createMeshNodeFromMeshInfo:(B3SFaceModel*) finalModel
{
  SCNNode *tempNode = [[SCNNode alloc] init];
  //load mesh .obj file into B3SFaceMesh isntance
  B3SFaceMesh *B3SFaceMesh = [finalModel getMeshInfo];
  // create scenekit geometry from B3SFaceMesh internal arrays
  SCNGeometry *mesh=[self createGeometryFromMeshInfo:B3SFaceMesh];
  tempNode.geometry = mesh;
  
  // load texture
  UIImage *diffuseTexImage = [finalModel getMeshImage];
  SCNMaterial *mat = [SCNMaterial material];
  mat.diffuse.contents = diffuseTexImage;
  tempNode.geometry.materials = @[mat];
  return tempNode;
}

// create a scenekit geometry from B3SFaceMesh
-(SCNGeometry *)createGeometryFromMeshInfo:(B3SFaceMesh *) meshInfo
{
  //vertex texture coordinate array
  int vtCount=(int)meshInfo.texCoords.count;
  float* vtArray = malloc(sizeof(float)*2 * vtCount);
  for (int i = 0; i < vtCount; ++i) {
    B3SPoint2f * vt=[meshInfo.texCoords objectAtIndex:i];
    vtArray[2*i]=vt.x;
    vtArray[2*i+1]=vt.y;
  }
  NSMutableData *vtData = [NSMutableData dataWithBytes:vtArray length:vtCount * sizeof(float) * 2];
  free(vtArray); vtArray=NULL;
  SCNGeometrySource *vtSource = 
        [SCNGeometrySource geometrySourceWithData:vtData
                        semantic:SCNGeometrySourceSemanticTexcoord
                     vectorCount:vtCount
                 floatComponents:YES
             componentsPerVector:2
               bytesPerComponent:sizeof(float)
                      dataOffset:0
                      dataStride:sizeof(float) * 2];
  
  // vertex position array
  int vCount=(int)meshInfo.vertices.count;
  SCNVector3 *vposArr = malloc(sizeof(SCNVector3) * vCount);
  for(int i=0; i<vCount; i++){
    B3SPoint3f *p=[meshInfo.vertices objectAtIndex:i];
    vposArr[i].x=p.x; vposArr[i].y=p.y; vposArr[i].z=p.z;
  }
  SCNGeometrySource *vertexSource = [SCNGeometrySource geometrySourceWithVertices:vposArr count:vCount];
  free(vposArr); vposArr=NULL;
  
  // vertex normal array
  int vnCount=(int)meshInfo.normals.count;
  SCNVector3 *vnArr = malloc(sizeof(SCNVector3) * vnCount);
  for(int i=0; i<vnCount; i++){
    B3SPoint3f *p=[meshInfo.normals objectAtIndex:i];
    vnArr[i].x=p.x; vnArr[i].y=p.y; vnArr[i].z=p.z;
  }
  SCNGeometrySource *vnSource = [SCNGeometrySource geometrySourceWithNormals:vnArr count:vnCount];
  free(vnArr); vnArr=NULL;
  
  // triangle element array
  int fn=(int)meshInfo.faces.count;
  int * fArr=malloc(sizeof(int)*3* fn);
  for (int i=0; i<fn; i++) {
    fArr[3*i]=meshInfo.faces[i][0].x;
    fArr[3*i+1]=meshInfo.faces[i][1].x;
    fArr[3*i+2]=meshInfo.faces[i][2].x;
  }
  NSData *indexData = [NSData dataWithBytes:fArr length:fn *sizeof(int)*3];
  
  SCNGeometryElement *element = [SCNGeometryElement
                                 geometryElementWithData:indexData
                                 primitiveType:SCNGeometryPrimitiveTypeTriangles
                                 primitiveCount:fn
                                 bytesPerIndex:sizeof(int)];
  
  SCNGeometry *geometry = [SCNGeometry
             //geometryWithSources:@[vertexSource, normalsSource]
             geometryWithSources:@[vertexSource, vnSource, vtSource]
             elements:@[element]];
  return geometry;
}

@end
