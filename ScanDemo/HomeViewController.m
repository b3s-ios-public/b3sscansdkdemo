//
//  HomeViewController.m
//

#import "HomeViewController.h"
#import "ScanViewController.h"
#import <AVFoundation/AVCaptureDevice.h>

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        NSLog(@"camera access permission is granted?:%@", granted?@"YES":@"NO");
    }];
    
  //NSDictionary *infoDict=[[NSBundle mainBundle]infoDictionary];
  //NSString *verStr = infoDict[@"CFBundleShortVersionString"];
  //NSString *appBuildVer = infoDict[@"CFBundleVersion"];
  //NSString *appName = [infoDict objectForKey:@"CFBundleDisplayName"];
  //self.title=[appName stringByAppendingFormat:@" %@(%@)",verStr,appBuildVer];
}

+ (void)requestUseVideoCamera:(void(^)(BOOL isCanUse))CompletionHandler
{
    NSString *tipTextWhenNoPhotosAuthorization; // 提示语
    NSString *mediaType = AVMediaTypeVideo;     // 读取媒体类型
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType]; // 读取设备授权状态
    if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied) {
        NSDictionary *mainInfoDictionary = [[NSBundle mainBundle] infoDictionary];
        NSString *appName = [mainInfoDictionary objectForKey:@"CFBundleDisplayName"];
        tipTextWhenNoPhotosAuthorization = [NSString stringWithFormat:@"请在\"设置-隐私-相机\"选项中，允许%@访问你的手机相机", appName];
        // 展示提示语
        NSLog(@" -- %@ ",tipTextWhenNoPhotosAuthorization);
        if (CompletionHandler) {
            CompletionHandler(NO);
        }
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined) { // 第一次请求。
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (CompletionHandler) {
                    CompletionHandler(granted);
                }
            });
        }];
    } else {
         if (CompletionHandler) {
             CompletionHandler(YES);
         }
     }
 }
         
 + (void)openSystemSetting
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

- (IBAction)tapGotoScan:(id)sender {
  ScanViewController *vc=[ScanViewController new];
  [self.navigationController pushViewController:vc animated:YES];
}


@end
