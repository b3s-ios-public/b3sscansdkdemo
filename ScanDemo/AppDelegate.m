//
//  AppDelegate.m
//

#import "AppDelegate.h"
#import <B3SScanSDK/B3SSdkManager.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // get the singleton
    B3SSdkManager *sdkMgr = [B3SSdkManager sharedInstance];
    
    // request to get authorization. Please make sure the app have access to network beforehand.
    NSDate *start=[NSDate date];
    [sdkMgr verifySDKIntegrity:^(NSError *error){
        NSLog(@"verificate time used:%.3lf seconds",[[NSDate date] timeIntervalSinceDate:start]);
        if(error){
          NSLog(@"sdk init failed, %@", error);
        }else{
          NSLog(@"sdk init succ");
        }
    }];
    
    
    [self setLanguageSetting];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


-(void) setLanguageSetting
{
  //if (![[NSUserDefaults standardUserDefaults]objectForKey:@"appLanguage"]) {
    //NSArray *languages = [NSLocale preferredLanguages];
    //NSString *language = [languages objectAtIndex:0];
    
    NSArray *languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    NSString *firstLang = languages.firstObject;
    
    if ([firstLang hasPrefix:@"zh-"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"zh" forKey:@"appLanguage"];
    } else {//if([language hasPrefix:@"en"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"appLanguage"];
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
  //}
}

@end
