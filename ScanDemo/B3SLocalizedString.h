#ifndef B3SLocalizedString_H
#define B3SLocalizedString_H


#define B3SLocalizedString(key, comment)  [[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"appLanguage"]] ofType:@"lproj"]] localizedStringForKey:(key) value:nil table:@"Localizable"]


#endif /* B3SLocalizedString_H */
