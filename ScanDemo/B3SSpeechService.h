//
//  B3SSpeechService.h
//
//

#import <Foundation/Foundation.h>

@interface B3SSpeechService : NSObject

+ (instancetype)sharedInstance;

- (BOOL)speak:(NSString *)speech;

- (BOOL)speak:(NSString *)speech force:(BOOL)isForce;

@end


