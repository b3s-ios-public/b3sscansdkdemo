# 3D Face Scanning SDK


###  iOS device requirement

+ iOS version：
    this SDK can only work on iOS 11.1+, check out it coverage here: https://developer.apple.com/support/app-store
+ Supported Device model：
    this SDK can only work on iOS devices equipped with Apple's FaceID, which are:
       iPhone X / Xs / Xs max / Xr / 11/ 11 Pro /11 Pro Max,
       iPad Pro 12.9-inch 3rd & 4th generation,
       iPad Pro 11-inch 1st & 2nd generation.



##  How to run this demo?

1. Get the license certificate for the SDK

You need to aquire a license certificate file to use this SDK  formally.
Please send an email to techsupport@body3dscale.com to apply for it. You might 
need to include necessary information about your organization and business, your 
website or the BundleID of your app or something alike.

2. You must have  cocoapods installed first, if not, please Google search Cocoapods to learn about it.
3. Then open your command line terminal, go to the root directory of this demo project, and execute `pod install`
4. Open the B3SScanSDKDemo.xcworkspace using Xcode
5. change the BundleID of B3SScanSDKDemo target to your app's BundleID.
    (you have to make your SDK version match with your BundleID, or else 
    SDK verification will fail.)
6. build and run




## How to use this B3SScanSDK.framework ?

### 1. Integrate the SDK into your app workspace/project

   you can copy the 'B3SScanSDK.framework' to the build directory of your own app. 
Just like this demo project, we simply put it at the source root directory, or wherever you like.

   Then open your app workpspace using Xcode, go to the 'Build Phase' of your app target, 
click the '+' button in the 'Link Binary With Libraries' section, then click 'Add Others...' and 
select 'Add Files...', navigate to the 'B3SScanSDK.Framework' and select it. 

   Finally, you have to add the 'B3SScanSDK.framework' to the 'Copy Files' section of 'Build Phase' tab,
make sure the 'Code Sign on Copy' box is checked. And add the 'det.mnn' and 'pts.mnn' 
to 'Copy Bundle Resources' section of 'Build Phase' tab. The result should be as follow:

![add copy files](add-files-to-Copy-section.jpg)


### 2. How to scan and reconstruct a 3D face?

 Please refer to the 'ScanViewController.mm' in this B3SScanSDKDemo project and 
 the related api documentation. 



### Contact us

+ mail: techsupport@body3dscale.com


Copyright @ 2021 body3dscale.com
