## 手机3D人脸建模SDK

## 3D人脸建模

### 介绍
本SDK包含基于苹果前置深度相机的3D扫描技术。

### 特性

+ 模型还原度高：模型纹理分辨率支持3K分辨率，模型还原度高，满足医美级应用要求。
+ 更多特性和领域：提供3D人脸五官轮廓及美学特征点，提供3D人脸五官测量数据，眼镜试戴，牙科DSD。

###  依赖及版本
+ 操作系统：支持iOS11及以上
+ 支持的终端：(带FaceID的iOS设备)
   iPhone X / Xs / Xs max / Xr / 11/ 11 Pro /11 Pro Max;
   iPad Pro 12.9-inch 3rd & 4th generation; 
   iPad Pro 11-inch 1st & 2nd generation;
   以及其他支持FaceID的苹果设备.


## 接入说明

### 获得授权license证书
+ 授权流程:
   请将你的需求和申请，以邮件形式发送至techsupport@body3dscale.com -> 等待我们联系您 -> 下发SDK包(含license证书)
+ 申请邮件的内容必须包含:
   公司全称及办公地址 或 个人开发者姓名、App的BundleID、联系人电话及邮箱

###  如何运行此demo ?
+ 从终端命令行进入本工程目录，执行pod install，然后打开B3SScanSDKDemo.xcworkspace
+ 进入B3SScanSDKDemo目标的General配置，修改Bundle ID为您的App bundle id.
+ 构建并在真机上运行


### 了解demo、掌握SDK基本用法
+ 主入口：ScanSDKDemo/HomeViewController.m
+ 扫描建模详细参见ScanViewController.m的实现代码

### 如何导入B3SScanSDK.framework到您自己的App工程 ?
+ 拷贝demo包目录下面的B3SScanSDK.framework到您的App工程下的源码目录
+ 用xcode打开您的App工程，在Build Phase->Link Binary With Libraries(链接库)中，
  点击+号，再点'Add Others..'(添加其他..)，找到拷贝好的B3SScanSDK.framework并添加
+ 在Build Phase的'Copy Files'中，添加B3SScanSDK.framework并勾选'Code Sign on Copy';
  再在'Copy Bundle Resources'中，添加det.mnn和pts.mnn，最终如下图所示:
  ![add copy files](add-files-to-Copy-section.jpg)


### 联系我们

+ 邮箱：techsupport@body3dscale.com

