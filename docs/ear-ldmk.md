## About Ear 3D Landmark

### How to make B3SScanSDK create Ear 3D landmarks ?

You must set `B3SScanSettings.enableEarLdmk` to YES,
and specify `B3SScanSettings.earLdmkServiceHost` to your ear landmark locating server


### How to get the ear landmark points array?

when the scanning process finished, you will get the directory path where mesh files saved,

```objective-c
B3SFaceModel *finalModel=[[B3SFaceModel alloc] initModelPath:self.modelDir];
B3SLandmark3D * landmark = [finalModel getLandmark3D];

// landmark.leftEarLdmks
//landmark.rightEarLdmks

```

### What is ldmk3d_ear.yml?
The ear landmark points are saved in this file.
You can turn it into a ldmk3d_ear.obj, and open it and align it with mesh.obj in MehsLab app, 
to visualise where the points are.

The following javascript utils can help you turn landmark points into vertex description in ldmk3d_ear.obj

```javascript
//1.copy the left and right ear landmark coordinates into your chrome console
//for example:
let left=[-7.95136392e-02, -8.25118721e-02, -8.50760415e-02,
       -8.65092501e-02, -8.95736516e-02, -8.98639038e-02,
       -8.93380865e-02, -8.76839384e-02, -8.53975713e-02,
       -8.48066956e-02, -8.37353393e-02, -8.23918879e-02,
       -8.09424222e-02, -7.79582411e-02, -7.58736730e-02,
       -7.37020522e-02, -7.07784519e-02, -6.81880862e-02,
       -6.59801215e-02, -6.43304363e-02, 3.47556430e-03, -1.81681709e-04,
       -3.05383443e-03, -2.88040424e-03, 1.44178909e-03, 6.50723884e-03,
       1.09189367e-02, 1.67337749e-02, 2.16956623e-02, 2.68167853e-02,
       3.26268934e-02, 3.75433080e-02, 4.17943895e-02, 4.62254658e-02,
       4.96905297e-02, 5.28977029e-02, 5.45073859e-02, 5.55908792e-02,
       5.51828966e-02, 5.20677604e-02, 4.04589742e-01, 4.09116030e-01,
       4.15281117e-01, 4.19740796e-01, 4.24505651e-01, 4.27519292e-01,
       4.29364651e-01, 4.30432647e-01, 4.29584324e-01, 4.28743362e-01,
       4.26573366e-01, 4.23998237e-01, 4.22153592e-01, 4.19291764e-01,
       4.16069180e-01, 4.12475705e-01, 4.09443021e-01, 4.05956775e-01,
       4.02226448e-01, 4.00788188e-01 ];
       
leftVert="";
for(let i=0; i<20; i++){
  leftVert += "v "+left[i]+" "+left[20+i]+" "+left[40+i]+"\n";
}

//2. the output will be:
v -0.0795136392 0.0034755643 0.404589742
v -0.0825118721 -0.000181681709 0.40911603
v -0.0850760415 -0.00305383443 0.415281117
v -0.0865092501 -0.00288040424 0.419740796
v -0.0895736516 0.00144178909 0.424505651
v -0.0898639038 0.00650723884 0.427519292
v -0.0893380865 0.0109189367 0.429364651
v -0.0876839384 0.0167337749 0.430432647
v -0.0853975713 0.0216956623 0.429584324
v -0.0848066956 0.0268167853 0.428743362
v -0.0837353393 0.0326268934 0.426573366
v -0.0823918879 0.037543308 0.423998237
v -0.0809424222 0.0417943895 0.422153592
v -0.0779582411 0.0462254658 0.419291764
v -0.075873673 0.0496905297 0.41606918
v -0.0737020522 0.0528977029 0.412475705
v -0.0707784519 0.0545073859 0.409443021
v -0.0681880862 0.0555908792 0.405956775
v -0.0659801215 0.0551828966 0.402226448
v -0.0643304363 0.0520677604 0.400788188

//3. just copy the above lines into ldmk3d_ear.obj
// refer to the `docs/ldmk3d_ear.yml` and `docs/ldmk3d_ear.obj` for the complete data.

``` 

