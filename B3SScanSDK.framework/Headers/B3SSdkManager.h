//
//  B3SSdkManager.h
//  B3SPlasticSimulationSDK
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
  @abstract
    SDK uniform authorization entry.
 */
@interface B3SSdkManager : NSObject

/**
  @abstract get single instance of this class
 */
+ (instancetype)sharedInstance;

//err is nil when everything is good. 
typedef void(^VerifyCallBack)(NSError* err);

/**
 @abstract verify sdk integrity.
 @param cb callback of verification, should not be nil
 */
- (void) verifySDKIntegrity:(VerifyCallBack) cb;



@end
