//
//  B3SFaceReport.h
//
//  Copyright © 2020 Body3DScale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
  @class B3SFaceReport
  @abstract 
    contains the real-time face detection result, landmarks points and several
    indicators determine whether the head pose of the user is suitable for starting scan.
  
  @discussion
    During scanning, only euler angles in this property are meaningful and refreshed
    in quasi real-time, other fields are random values.
    
    Before scan start(i.e. during video previewing), all the fields in 
    this property are meaningful and refreshed in realtime.
 */
@interface B3SFaceReport : NSObject

/**
  @property points
  @abstract landmark points. convert NSValue* to CGPoint* to read it.  
 */
@property (nonatomic, strong) NSArray <NSValue *> * points;

/**
 @abstract the bounding box of face rect
 */
@property (nonatomic, assign) CGRect rect;


/**
 @property imgWidth
 @abstract width of the color image
 */
@property (nonatomic, assign) int imgWidth;

/**
 @property imgHeight
 @abstract height of the color image
 */
@property (nonatomic, assign) int imgHeight;

/**
 @property pitch
 @abstract pitch of euler angle, in degree.
 whether the face image is horizontally mirrored or not, 
 it's going to the negative when the user's physical head is lowering down.
 it's going to the positive when the user's physical head is raising up.
 */
@property (nonatomic, assign) float pitch;

/**
  @property yaw
  @abstract yaw of euler angle, in degree.
  
  If the face image is horizontally mirrored(see `B3SScanner startCamera:withCompletion:`), 
  it's going to the negative when the user's physical head is turning to his right hand side.
  it's going to the positive when the user's physical head is turning to his left hand side.
  Otherwise, the reverse is true.
 */
@property (nonatomic, assign) float yaw;

/**
  @property roll
  @abstract roll of euler angle, in degree.
  
  If the face image is horizontally mirrored(see `B3SScanner startCamera:withCompletion:`), 
  then this angle is going to the negative when the user's physical head is tilting to his left shoulder.
  it's going to the positive when the user's physical head is tilting to his right shoulder.
  
  Otherwise, the reverse is true.
 */
@property (nonatomic, assign) float roll;

/**
 @property eulerAnglesInfo
 @abstract
    an indicator, indicates whether user's head pose is appropriate 
    for starting scan. if it's not 0, your app should disable
    the 'Start Scan' button in your UI and give proper tips to the user.
    
 @discussion
    available values for this property:
 @code
    * 0 - user's head pose is good for starting scan.
    * 1 - user's head is raised up too much. 
        tips for user: please lower your head a little.
    * 2 - user's head is lowered down too much.
        tips for user: please raise your head up a little.
    * 3 - user is facing to either the left or the right too much.
        tips for user: please look straight ahead to the camera.
    * 4 - user's head is tilted to the left or right shoulder too much.
        tips for user: please make your head upright.
 @endcode   
    人脸角度评估
    * 0：角度合适 
    * 1：仰头了（提示参考：请低头） 
    * 2：低头了（提示参考：请抬头） 
    * 3：左右转头（提示参考：请平视） 
    * 4：两边侧头（提示参考：请平视）
 */
@property (nonatomic, assign) int eulerAnglesInfo;


/**
  @property distanceInfo
  @abstract
    an indicator, indicates whether distance from user's head to the camera
    is appropriate for starting scan. if it's not 0, your app should disable 
    the 'Start Scan' button in your UI and give proper tips to the user.
  
  @discussion
    available values for this property:
    
  @code
    * 0 - the distance is good for starting scan.
    * 1 - user's head is too far from the camera.
          tips for the user: please stay closer to the camera.
    * 2 - user's head is too close to the camera.
          tips for the user: please stay further from the camera.
  @endcode
    人脸距离远近评估（0：距离合适 1：距离太远 2：距离太近）
 */
@property (nonatomic, assign) int distanceInfo;

/**
  @property hairInfo
  @abstract
    an indicator, indicates whether the user's hair style is appropriate for 
    starting scan. BUT currently this indicator is UNAVAILABLE.
  
  @discussion
    available values for this property:
    
  @code
    * 0 - user's hairstyle is good for scanning.
    * 1 - the output face model might be bad since user's 
          hairstyle is inappropriate for scanning.
  @endcode  
    头发评估（0：头发合适 1：头发太长）
 */
@property (nonatomic, assign) int hairInfo;

/**
  @property distance
  @abstract
    the distance from head center to the camera, in meter.
 */
@property (nonatomic, assign) float distance;

@end
