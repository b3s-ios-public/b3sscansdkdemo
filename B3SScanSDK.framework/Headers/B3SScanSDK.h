//
//  B3SScanSDK.h
//  B3SScanSDK
//
//  Copyright © 2020 Body3DScale. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <B3SScanSDK/B3SPoints.h>
#import <B3SScanSDK/B3SSdkManager.h>
#import <B3SScanSDK/B3SFaceReport.h>
#import <B3SScanSDK/B3SScanQuality.h>
#import <B3SScanSDK/B3SFaceModel.h>
#import <B3SScanSDK/B3SScanSettings.h>
#import <B3SScanSDK/B3SScanner.h>

//! Project version number for B3SScanSDK.
FOUNDATION_EXPORT double B3SScanSDKVersionNumber;

//! Project version string for B3SScanSDK.
FOUNDATION_EXPORT const unsigned char B3SScanSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <B3SScanSDK/PublicHeader.h>


