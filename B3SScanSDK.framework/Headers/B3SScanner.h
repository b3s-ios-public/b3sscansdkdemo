//
//  B3SScanner.h
//
//  Copyright © 2020 Body3DScale. All rights reserved.
//
#pragma once

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreGraphics/CoreGraphics.h>
@class B3SScanSettings;
@class B3SFaceReport;
@class B3SScanQuality;
@class B3SFaceModel;

/*!
  @class B3SScanner
  @brief
      The main manager which controls the camera session, scaning session.
      
  @discussion
  Typicall usage:
  1. when your app enter the face scanning UI view controller(designed by you),
  you should first creat a B3SScanner instance in viewDidLoad callback and 
  retain it. Then call B3SScanner `startCamera` method to open 
  camera stream in viewDidAppear or viewWillAppear. 
  In the viewDidDisappear callback, you should call B3SScanner 
  closeCamera method. This instance should be destroyed once your view 
  controller is dealloced. 

  2. -Before scan started-
    Your app have to guide the user to adjust his/her head pose so that the 
    scanning could be started. Specifically, once the startCamera is called,
    the video stream from camera is presented in the preview layer attached
    to your UI. The B3SScanner instance will internally keep detecting 
    the user's face and determine if his/her head pose is suitable 
    for scanning. Generally speaking, the B3SScanner allows starting scan
    when the user is looking straight to the camera with the head upright
    and the distance from the frontal face to the camera is nearly the normal
    reading distance. Those conditions checking is done by B3SScanner and 
    the results will be saved as several indicators of `faceInfo` property. 
    You need to check those indicators and enable user to start scanning (e.g. 
    enable 'Start Scan' button) when all indicators are 0. 
    
    Once all the conditions are met, you can guide the user to start scan and
    call B3SScanner `startScan` in response to user's 'start scan' action.
  
  3. -During scanning-
    You might need to design some UI indicators to instruct your users how to
    turn his/her head during the scanning process like the Beauty3D app did.
    The following is a typical way:
    - instruct the user, via voice,text or animations, to turn the head in the
      following sequence:
      begin at frontal face --> slowly turn to the left --> slowly back to the middle
      -->slowly turn to the right-->slowly back to the middle.
      
    - you can observe the yaw angle in the `faceInfo` during scanning to decide 
      when and which voice/text/animation instruction should be played.
      
      We provide a simple source code demo in the SDK package 
      to show the above typical usage. More questions are welcome.
      
  4. -Scanning finished-
    Generally, the scanning will be automatically stopped by B3SScanner when
    the user turn his/her head back to the middle finally. Then the B3SScanner
    will execute the postprocessing steps and update its progress in the 
    'genModelProgress' property. 
    
  5. -Postprocessing finished-
    The postprocessing steps usually take ten seconds or so to finish. Once 
    finished, the `finishHandleWithModelPath` will be invoked to inform you 
    where the final model files are saved.
 */
@interface B3SScanner : NSObject

/**
  @property scanSettings
  @brief
    configuration values for the face scanning algorithm.
  @discussion
    each configuration field in the settings has a default value.
    You have to change them(if you want) before starting scan.   
  @see B3SScanSettings.h
 */
@property (nonatomic, strong) B3SScanSettings *scanSettings;

/**
 @property faceInfo
 @brief
    real-time face detection result. 
 
 @discussion
    this property is updated in realtime once camera is opened. 
    You can observe it by KVO, but you should not retain this property 
    out of the scope of the KVO observer. Instead, we suggest to deeply copy
    its members in the KVO observer and use the copied ones anywhere you want.
    
 @see B3SFaceReport.h
 */
@property (atomic, strong) B3SFaceReport *faceInfo;

/**
  @property scanInfo
  @brief
    a summary for the scaning process, including head movement assessment,
    how good the output face model is.
  
  @discussion
      This property is only available after the scanning process is finished
      normally. You should never create B3SScanQuality instance by yourself.
      扫描过程信息（扫描状态、得分等信息）
      
  @see B3SScanQuality.h
 */
@property (nonatomic, strong) B3SScanQuality *scanInfo;

/**
  @property modelInfo
  @brief 
    this property contains a directory path where the final face model is saved,
    and some utilities to read mesh, 3d landmark vertices and texture image.
    
  @discussion
    this property is only available after the scanning process is 
    finished normally. You should never create B3SFaceModel instance by yourself.
    
    模型信息（包含顶点、纹理、面等信息）.
  @see B3SFaceModel.h
 */
@property (nonatomic, strong) B3SFaceModel *modelInfo;

/**
  @property genModelProgress
  @brief
    the progress indicator of postprocessing steps, 1.0 means the postprocessing
    is done, means the final model will be ready very soon.
  @discussion
    this property is currently not KOV observable. If you want to show 
    a progress bar to the user, you have to read it proactively and repeatly.
    
    模型生成进度（模式生成百分比）
 */
@property (nonatomic, assign) float genModelProgress;

/**
  @property abortWithError
  @brief
    it's a callback you should implement to capture SDK internal error.
    In some rare case, the scanning algorithm run into an error during
    scanning or postprocessing. 
  @discussion
    This is mainly for debugging. However, in some rare cases, 
    e.g. the user turns his/her head very fast or abnormally,  
    the scanning algorithm can not move on. 
    When the algorithm does run into an error, this method is invoked on 
    main thread. The `desc` argument is an error description just for debugging,
    not for showing to your user. Since all the errors can not be recovered, 
    we suggest you to simply show a guide message to instruct your user to 
    start over.
    
    SDK内部发生异常情况时的回调函数.
 */
@property (nonatomic, copy) void(^abortWithError)(int code, NSString *desc);


/**
@property  finishHandleWithModelPath
@brief
   This callback is invoked on main thread after the scanning and
   postprocessing are finished. If `error` is nil, then the `modelDir`
   is a directory containing all the final model files;  alternatively, you
   can also access the final model files from the `modelInfo` property.

   if `error` is not nil, then the `modelDir` will be nil and it means
   SDK authorization failure, if you get trapped in this situation, 
   please contact us.
*/
@property (nonatomic, copy) void(^finishHandleWithModelPath)(NSString *modelDir, NSError * error);

/**
  @method isScanStarted
  @brief
      indicate whether it is now in the scanning process.
  
  @return YES - it's in the scanning process; 
          NO - it's before scanning or it's during postprocessing 
          or everything is cancelled. 
 */
-(BOOL) isScanStarted;

/**
  @method isScanCancelled
  @brief
    indicate whether the scanning process or postprocessing is cancelled.
    
  @return YES - all cancelled; NO - scanning is not started or in progress. 
 */
-(BOOL) isScanCancelled;


/**
  @method startScan
  @brief start scanning. your app should call this when deciding 
  to start scanning, e.g. the user clicks the 'Start Scan' button in your app.  
  
  @return if the user's head pose is inappropriate for scanning，
    it return an NSError instance, or else return nil.
  
  @discussion
  this method internally will check again whether the head pose is 
  appropriate for scanning, if not, it returns an NSError with following possible
  codes and messages: 
  @code
   code        description
   101    no human face detected.
   102    distance from your head to the camera is beyond normal reading distance range.
   103    please look straight to the camera with your head and neck upright.
   104    adjust the distance between head and the camera, and look at the camera.
   105    authorization failure. please retry authorization API or contact us.
  @endcode
 */
- (NSError *)startScan;

/**
 @brief 
 check if the user's head pose is suitable for starting scan.
 @return see `startScan`
 */ 
-(NSError*)checkIfCouldStart;

/**
 * @brief end scanning process and start the postprocessing.
 */
- (void)stopScan;


typedef void(^CancelCallBack)(void);
/**
  @brief asynchronously cancel every task running in the background, 
    include the face detection, scanning and postprocessing.
  @param cb it will be called on main thread when all tasks are finalized.
 */
- (void)cancel:(CancelCallBack)cb;

/**
 @brief 
   delete the whole `B3SFaceModel.modelPath` directory .
 @discussion
    you can use this method to remove the produced model files.
 */
- (BOOL)deleteModeInfo;


// MARK: -- camera --

/**
  @brief
    your app must pass in a layer of a UIView which is used for presenting 
    camera video stream, namely the stream preview layer.
  
  @param previewRootLayer a layer of some UIView of your app, 
        used for presenting camera video stream, its width:height must be 3:4  
*/
- (void) setCameraPreviewRootLayer:(CALayer *)previewRootLayer;

/**
 @brief same as `startCamera:`, but the mirror argument is set 
  to NO internally, meaning pictures in the video won't be horizontally flipped.
 @param completionCB same as `startCamera:withCompletion:`
*/
- (void) startCamera:(void(^)(NSError* error))completionCB;

/**
  @brief 
     open the front camera, show the video stream in the preview layer,
     and prepare some resources for scanning.
  
  @param mirror 
     indicating whether the video should be mirrored about its vertical axis. 
     When set it to YES, the user looks at the resulting face model as if he/she 
     was looking at him/herself in the mirror; when set this argument to NO, 
     the final model is a Third-person-perspective model, it probably looks 
     strange to the user him/herself, but it looks familiar to his/her friends.  
     see `AVCaptureConnection.videoMirrored` in iOS documentation for details.
  @param block this block will be invoked on main thread when this call finish. 
    
  @discussion
    This method will internally request the camera access permission from 
    the user if your app isn't granted this permission beforehand. We suggest
    you to prepare and request this permission before opening camera.
    
    The returned NSError by the `block` has the following possible
    codes and messages:
    
  @code
    10000  -the camera was already opened, please `stopCamera` first.
    10001  -camera access permission is not determined.
    10002  -the camera on this device doesn't support 3D scanning,
            use the iPhones/iPads that support FaceID instead.
    10003  -the camera doesn't support video format desried by this SDK.
    10004  -the video output configurations failed. please contact us.
    10005  -the user explicitly refused to grant camera access permission.
    10006  -failed to create camera device intpu. please contact us.
    10007  -no preview layer(a CALayer instance) was specified.
    10008  -this error unlikely occur
  @endcode    
 */
- (void) startCamera:(BOOL)mirror
      withCompletion:(void(^)(NSError* error))block;

/**
  @brief close the camera.
  @discussion 
  this will close the camera capture session, and detach the preview 
  layer from its parent view, but not release the resources used for scanning. 
  To release those scanning related resources, set your B3SScanner instance
  to nil instead(usually, your B3SScanner instance is dealloced when the 
  scan view controller is popped out of navigation controller).
 */
- (void) stopCamera;

/**
  @brief
    restart the capture session without touching any configuration 
    of the session. But it does reset internal resource states of 
    the scanning algorithm.
    
  @discussion
    This method is useful in the implementation of `abortWithError:` callback, 
    you can restart the camera and reset all the internal state by 
    calling this API there. It has no side effects when invoked 
    before starting scan.
 */
- (void) restartCamera;

/**
  @brief 
    get the frame rect of the video stream preview layer.
  
  @return
    return the rect of the preview layer.
*/
-(CGRect) getCameraPreviewLayerFrameRect;

/**
 @brief 
    update the face bounding box sublayer on the preview layer.
 
 @param rect the rect of face boundingbox
 
 @discussion
  This api is used for debugging purpose.
  if you want to show the face box, you call this method.
  please refer to the demo source code for how to do this.
 */
-(void)setCameraPreviewFaceboxRect:(CGRect)rect;


@end


